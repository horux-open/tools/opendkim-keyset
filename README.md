# Opendkim Keyset

## Running this program can damage your server, use it at your own risk!

## Description
Create key pairs for OPENDKIM email signature. This tool is specially useful in the operation of multi-domain e-email servers that use OPENDKIM to sign e-mail. It assumes that both Singing Tables and Key Tables are enables. 

The tool creates:
   - The private key
   - The TXT DNS record containing the public key that should be configured in the DNS service.
   - The Singing Table entry for the new domain and selector
   - The Key Table entry

## Installation
Create the jar file

    $ mvn compile

Copy the jar file to any working directory

    $ cp target/opendkim-kset-X.X.jar ~/opendkim-kset/

Create a configuration file

    $ touch ~/opendkim/opendkim-ksey.conf

Add the following to the configuration file 
    
    keys_path=<The base directory of the dkim keys, usually /etc/opendkim/keys>
    key_length=<The desired private key length>
    key_owner_user=<User name, usually opendkim>
    key_owner_group=<Group name, usually opendkim>


## Usage

Change to the installation directory

    $ cd ~/opendkim-kset/

Run the program

    $ sudo java -jar opendkim-kset-X.X.jar <domain-name> [<selector>]

If selector is not specified the "default" selector is assumed.

The fileset will be created under 

    <keys-path>/<domain-name>/
        - <selector>.private                   # The private key
        - <selector>.domainkey.<domain-name>   # The DNS TXT record details
        - <selector>.KeyTable                  # The new entry to be added to the KeyTable file
        - <selector>.SingingTable              # The new entry to be added to the SingingTable file  


## Roadmap

Future palned features:

    - Report an error if the fileset cannot be created or there are not enough access permissions.
    - Set up proper file access permisions to the private key
    - Set up owner of the private key
    - Automatically update Key Table and SingingTable

## License
This project is released under the MIT license.

Using this program can result damage to your system and information loss. I cannot guarantee that the program is free of bugs. Use at your own risk!







