package com.horux.tools.opendkim;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author victormp
 */
public class KeysetConfig extends Properties{
   public KeysetConfig(String filename) throws IOException{
        try (FileInputStream fis = new FileInputStream(filename)) {
            load(fis);     
        } catch (IOException ex) {
           throw ex;
        }
    }
   
   public String getKeysPath(){
       return this.getProperty("keys_path");
   }
   
   
   public int getKeyLength(){
       return Integer.parseInt(this.getProperty("key_length"));
   }
}
