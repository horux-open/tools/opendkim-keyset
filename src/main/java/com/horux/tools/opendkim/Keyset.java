/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.horux.tools.opendkim;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author victormp
 */
public class Keyset {

    private static final String defaultPropertiesFileName = "opendkim-keyset.conf";
    static KeysetConfig keysetConfig;
    static String domain;
    static String selector;



    

    public static void main(String args[]) throws NoSuchAlgorithmException, IOException {
        keysetConfig = new KeysetConfig(defaultPropertiesFileName);
        String keysetPath = keysetConfig.getKeysPath();
        int keyLength = keysetConfig.getKeyLength();
        if (!parseArgs(args)) return;
       
        KeysetPair ksp = new KeysetPair(keyLength, keysetPath, domain, selector);
        
        ksp.createFiles();

    }
    
    static private Boolean parseArgs(String args[]){
        if (args.length == 0){
            usage();
            return false;
        }
        domain = args[0];
        
        if (args.length == 1){
            selector = "default";
        } else {
            selector = args[1];
        }     
        return true;     
    }

    private static void usage() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
