/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.horux.tools.opendkim;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 *
 * @author victormp
 */
public class KeysetPair {
    
    KeyPair kp;
    String keysetPath;
    String domain;
    String selector;
    String domainPath;
    String recordName;
    String privateKeyPath;
    
    
    

    private void writePrivateKey() throws IOException {
        Key pvt = kp.getPrivate();
        Base64.Encoder encoder = Base64.getEncoder();

        
        
        privateKeyPath = String.format("%s/%s.private", domainPath,selector);
        String pvta []= encoder.encodeToString(pvt.getEncoded()).split("(?<=\\G.{64})");
        try (Writer out = new FileWriter(privateKeyPath)) {
            out.write("-----BEGIN RSA PRIVATE KEY-----\n");
            for (String ln : pvta){
                out.write(ln+"\n");
            }
            out.write("-----END RSA PRIVATE KEY-----\n");
        }

    }

    private void writePublicKey() throws IOException {
        Key pub = kp.getPublic();
        Base64.Encoder encoder = Base64.getEncoder();

        String outFile = String.format("%s/%s.public", domainPath,selector);
        String puba []= encoder.encodeToString(pub.getEncoded()).split("(?<=\\G.{64})");
        try (Writer out = new FileWriter(outFile)) {
            out.write("-----BEGIN PUBLIC KEY-----\n");
            for(String ln: puba){
                out.write(ln+"\n");
            }
            out.write("-----END PUBLIC KEY-----\n");
        }

    }
    
    private void writeRecord() throws IOException{
        Key pub = kp.getPublic();
        Base64.Encoder encoder = Base64.getEncoder();
        String outFile = String.format("%s/%s", domainPath,recordName);
        try (Writer out = new FileWriter(outFile)){
            out.write("Name: "+ recordName +"\n\n");
            out.write("Value: v=DKIM1; k=rsa;  p=" + encoder.encodeToString(pub.getEncoded()));
        }
        
    }
    
    private void writeSingingTable() throws IOException {
        String outFile = String.format("%s/%s.SingingTable", domainPath,selector);
        try (Writer out = new FileWriter(outFile)){
            String stEntry = String.format("*@%s %s", domain, recordName);
            out.write(stEntry);
            
        }
    }
    
    private void writeKeyTable() throws IOException {
        String outFile = String.format("%s/%s.KeyTable", domainPath,selector);
        try (Writer out = new FileWriter(outFile)){
            String stEntry = String.format("%s:%s:%s", domain, selector, privateKeyPath);
            out.write(stEntry);
            
        }
    }
    
    
    public KeysetPair(int size, String keysetPath, String domain, String selector) throws NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(size);
        kp = kpg.generateKeyPair();
        this.keysetPath = keysetPath;
        this.domain = domain;
        this.selector = selector;
        
        this.domainPath = String.format("%s/%s", keysetPath, domain);
        this.recordName = String.format("%s._domainkey.%s", selector,domain);
    }
    
    
    private boolean isDomainPathAvailable(){
        
        File dpathFile = new File(domainPath);
        if (! dpathFile.exists()){
            return dpathFile.mkdirs();
        }
        return true;    
    }
    
    public boolean createFiles() throws IOException{
        if (!isDomainPathAvailable()) return false;
        writePrivateKey();
        writePublicKey();
        writeRecord();
        writeSingingTable();
        writeKeyTable();
        
        
        return true;
        
    }

    

    
    
    
}
