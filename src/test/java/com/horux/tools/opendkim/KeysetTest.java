/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.horux.tools.opendkim;


import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Assertions;

/**
 *
 * @author victormp
 */
class KeysetTest {
    
    ClassLoader classLoader =   getClass().getClassLoader();
    String resourceName = "opendkim-keyset.conf";
    

    
   KeysetConfig getKeysetConfig() throws IOException{
       String file = classLoader.getResource(resourceName).getFile();
       return new KeysetConfig(file);
   }
    
    
    @Test
    void loadConf() throws IOException{
        KeysetConfig keysetConfig = getKeysetConfig();
        Assertions.assertNotNull(keysetConfig);
    }
    
    @Test 
    void testConfiguration() throws IOException{
        KeysetConfig keysetConfig = getKeysetConfig();
        Assertions.assertEquals("/etc/opendkim/keys", keysetConfig.getKeysPath());
        Assertions.assertEquals(4096, keysetConfig.getKeyLength());
    }
    
 
    @Test
    void testCreateExampleCom() throws IOException, NoSuchAlgorithmException{
        KeysetConfig keysetConfig = getKeysetConfig();
        String domain = "example.com";
        String selector = "default";
        int keyLength = keysetConfig.getKeyLength();
        String keysetPath = keysetConfig.getKeysPath();
        KeysetPair ksp = new KeysetPair(keyLength, keysetPath, domain, selector);
        ksp.createFiles();
        
    }
    
    
}
